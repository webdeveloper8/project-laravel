<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Register</title>
</head>
<body>
    <h1>Create New Account</h1>
    <h2>Form Sign Up</h2>
    <form action="/send" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name :</label><br>
        <input type="text" name="lastname"><br><br>
        <label for="">Gender :</label><br><br>
        <input type="radio" id="male" name="gender"><label for="male" >Male</label><br>
        <input type="radio" id="female" name="gender"><label for="female" >Female</label><br><br>
        <label for="">Nationality :</label><br><br>
        <input type="checkbox" id="Indonesia"><label for="indonesia">Indonesia</label><br>
        <input type="checkbox" id="america"><label for="america">America</label><br>
        <input type="checkbox" id="spanyol"><label for="spanyol">Spanyol</label><br><br>
        <label for="">Bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <button type="submit">Sign Up</button>
    </form>
</body>
</html>